package com.jlngls.ricicleview.Activity.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jlngls.ricicleview.Activity.Adapter.Adapter;
import com.jlngls.ricicleview.Activity.RecyclerItemClickListener;
import com.jlngls.ricicleview.Activity.model.Filme;
import com.jlngls.ricicleview.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
private List<Filme> listaFilmes = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.recyclerViewID);

        //criar lista
         criarfilmes();
        // configurar adapter
        final Adapter adapter = new Adapter(listaFilmes);

        // configurar recicleview
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this,LinearLayout.VERTICAL));
        recyclerView.setAdapter(adapter);

        //evento de click
        recyclerView.addOnItemTouchListener(
new RecyclerItemClickListener(getApplicationContext(),
        recyclerView,
        new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Filme filme = listaFilmes.get(position);
                Toast.makeText(MainActivity.this, "O " + filme.getFilmes() + " foi Selecionado!", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onLongItemClick(View view, int position) {
                Filme filme = listaFilmes.get(position);
                Toast.makeText(MainActivity.this, "O " +filme.getFilmes() + " foi pressionado", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



            }
        })
        );


    }

    public void criarfilmes(){
        Filme filme = new Filme("Homem-Aranha","2017","Ação");
        listaFilmes.add(filme);

        filme = new Filme("A Bela e a Fera","2017","Romance/Ação");
        listaFilmes.add(filme);

        filme = new Filme("Vingadores Ultimate","2019","Ação/Fixão");
        listaFilmes.add(filme);

        filme = new Filme("Vingadores War Infinite","2017","Ação/Fixão");
        listaFilmes.add(filme);

        filme = new Filme("Velores & Furiosos 8","2016","Ação/Fixão");
        listaFilmes.add(filme);

        filme = new Filme("Dragon Ball Broly","2018","Desenho");
        listaFilmes.add(filme);

        filme = new Filme("Coringa","2019","Comédia/Suspense");
        listaFilmes.add(filme);

        filme = new Filme("Eu sou a lenda","2002","Suspense/Açãoo");
        listaFilmes.add(filme);

        filme = new Filme("Bad boys","2020","Comédia/Ação");
        listaFilmes.add(filme);

        filme = new Filme("Anabelle","2018","Ação");
        listaFilmes.add(filme);

        filme = new Filme("odo mundo em panico 5","2012","Suspense/Comédia");
        listaFilmes.add(filme);


    }
}
