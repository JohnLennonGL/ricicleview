package com.jlngls.ricicleview.Activity.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jlngls.ricicleview.Activity.model.Filme;
import com.jlngls.ricicleview.R;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.mViewHolder> {

private  List<Filme> listaFilme;

    public Adapter(List<Filme> lista) {
        this.listaFilme = lista;

    }

    @NonNull
    @Override
    public mViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemlista = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.adaptador,parent,false);
        return new mViewHolder(itemlista);
    }

    @Override
    public void onBindViewHolder(@NonNull mViewHolder holder, int position) {
        Filme filme = listaFilme.get(position);
        holder.titulo.setText(filme.getFilmes());
        holder.ano.setText(filme.getDataAno());
        holder.genero.setText(filme.getGenero());



    }

    public void click(View view){ ;

    }

    @Override
    public int getItemCount() {
        return listaFilme.size();
    }

    public class mViewHolder extends RecyclerView.ViewHolder {

        TextView titulo;
        TextView ano;
        TextView genero;

        public mViewHolder(@NonNull View itemView) {
            super(itemView);

            titulo = itemView.findViewById(R.id.tituloID);
            ano = itemView.findViewById(R.id.anoID);
            genero = itemView.findViewById(R.id.generoID);
        }
    }
}
