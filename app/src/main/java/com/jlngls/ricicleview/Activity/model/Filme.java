package com.jlngls.ricicleview.Activity.model;

public class Filme {

    private String filmes;
    private String dataAno;
    private String genero;



    public Filme(String filmes, String dataAno, String genero) {
        this.filmes = filmes;
        this.dataAno = dataAno;
        this.genero = genero;
    }



    public  Filme(){

    }
    public String getFilmes() {
        return filmes;
    }

    public void setFilmes(String filmes) {
        this.filmes = filmes;
    }

    public String getDataAno() {
        return dataAno;
    }

    public void setDataAno(String dataAno) {
        this.dataAno = dataAno;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
